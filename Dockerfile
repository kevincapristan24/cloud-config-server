FROM java:latest
EXPOSE 8091
ADD  app.jar common-config-server.jar
ENTRYPOINT ["java","-jar","common-config-server.jar"]
